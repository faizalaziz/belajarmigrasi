@extends('layout.master')
@section('titleWeb')
    Tambah Cast
@endsection
@section('pageTitle')
    Tambahkan Cast Baru
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">-</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/cast" method ="post">
            @csrf
            <div class="form-group">
                <input type="text" id="nama" name="nama" placeholder="Nama Cast" class="form-control">
            </div>
            @error('nama')
                <div class="alert alert-danger">Nama harus diisi!</div>
            @enderror
            <div class="form-group">
                <input type="number" id="umur" name="umur" placeholder="Umur" class="form-control">
            </div>
            @error('umur')
                <div class="alert alert-danger">Umur harus diisi!</div>
            @enderror
            <div class="form-group">
                <textarea type="text" id="bio" name="bio" class="form-control" placeholder="Bio" cols="30" rows="10"></textarea>
            </div>
            @error('bio')
                <div class="alert alert-danger">Bio harus diisi!</div>
            @enderror
            <div class="form-group">
                <button type="submit" class="btn btn-success">TAMBAH</button>
            </div>
        </form>
    </div>
</div>
@endsection