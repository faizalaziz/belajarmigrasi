@extends('layout.master')
@section('titleWeb')
    CAST PROFILE
@endsection
@section('pageTitle')
    <b>Cast {{$cast_id->nama}}</b>
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">PROFIL CAST</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
        <a href="/cast" class="btn btn-success btn-sm" role="button">KEMBALI</a>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h5><b>Umur</b></h5>
        <p>{{$cast_id->umur}}</p>
        <hr>
        <h5><b>Bio</b></h5>
        <hr>
        <p>
            {{$cast_id->bio}}
        </p>
    </div>
</div>
@endsection