@extends('layout.master')
@section('titleWeb')
    Edit Cast
@endsection
@section('pageTitle')
    Edit Cast
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">-</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/cast/{{$cast_edit->id}}" method ="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <input type="text" id="nama" name="nama" value={{$cast_edit->nama}} class="form-control">
            </div>
            @error('nama')
                <div class="alert alert-danger">Nama harus diisi!</div>
            @enderror
            <div class="form-group">
                <input type="number" id="umur" name="umur" value={{$cast_edit->umur}} class="form-control">
            </div>
            @error('umur')
                <div class="alert alert-danger">Umur harus diisi!</div>
            @enderror
            <div class="form-group">
                <textarea type="text" id="bio" name="bio" class="form-control">{{$cast_edit->bio}}</textarea>
            </div>
            @error('bio')
                <div class="alert alert-danger">Bio harus diisi!</div>
            @enderror
            <div class="form-group">
                <button type="submit" class="btn btn-success">SIMPAN PERUBAHAN</button>
            </div>
        </form>
        <a href="/cast" class="btn btn-warning">BATALKAN PERUBAHAN</a>
    </div>
</div>
@endsection