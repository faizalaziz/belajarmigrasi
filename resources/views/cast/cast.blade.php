@extends('layout.master')
@section('titleWeb')
    Data Cast
@endsection
@section('pageTitle')
    Data Cast
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
        <a href="/cast/create" class="btn btn-success" role="button">Tambah Cast</a>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
     <table class="table table-bordered">
        <thead>                  
            <tr>
                <th width="5%">ID</th>
                <th width="70%">Nama</th>
                {{-- <th>Umur</th>
                <th>Bio</th> --}}
                <th width="25%">Action</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($dataCast as $key => $cast)
            <tr>
                <th>{{$key + 1}}</th>
                <th>{{$cast->nama}}</th>
                {{-- <th>{{$cast->umur}}</th>
                <th>{{$cast->bio}}</th> --}}
                <th>
                  <a href="/cast/{{$cast->id}}" class="btn btn-primary">Detail</a>
                  <a href="/cast/{{$cast->id}}/edit" class="btn btn-primary">Edit</a>
                  <form action="/cast/{{$cast->id}}" method="POST" style="display:inline-block">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger delete-confirm" name="delete">
                  </form>
                </th>
            </tr>
        @empty
            <tr>
              <td colspan="3">TIDAK ADA DATA</td>
            </tr>
        @endforelse
        </tbody>
     </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script>
$('.delete-confirm').click(function(event) {
     var form =  $(this).closest("form");
     var name = $(this).data("name");
     event.preventDefault();
     swal({
         title: `Apakah anda yakin akan menghapus field ini?`,
         text: "Jika kamu menghapus field ini, datanya akan hilang selamanya",
         icon: "warning",
         buttons: true,
         dangerMode: true,
     })
     .then((willDelete) => {
       if (willDelete) {
         form.submit();
       }
     });
 });

</script>
@endpush