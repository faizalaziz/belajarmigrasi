<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/table', function () {
    return view('table');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});


//ROUTE UNTUK CRUD

//CREATE DATA CAST
Route::get('/cast/create', [CastController::class,'create']);
//MENGIRIM DATA CAST KE DATABASE
Route::post('/cast', [CastController::class,'store']);

//READ
//MENAMPILKAN SEMUA DATA CAST (READ)
Route::get('/cast', [CastController::class,'index']);
//MENAMPILKAN DATA DENGAN ID TERTENTU
Route::get('/cast/{cast_id}', [CastController::class,'show']);

//UPDATE
//UPDATE DENGAN ID TERTENTU
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
Route::put('/cast/{cast_id}', [CastController::class,'update']);


//DELETE DENGAN ID TERTENTU
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);








